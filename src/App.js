import './App.css';
import AddTodos from './components/AddTodos'
import TodoList from './components/TodoList'
import { useState } from 'react'



function App() {

    const [todos, setTodos] = useState([])
    const [input, setInput] = useState("")

  return (
    <div className="App">
      <AddTodos input={input} setInput={setInput} setTodos={setTodos} todos={todos} />
      <TodoList todos={todos} setTodos={setTodos}/>
    </div>
  );
}

export default App;


/* 
01) Todo list app
02) input todo
03) show the list of todos
04) edit and delete
*/
