import React from 'react'


const TodoList = ({ todos, setTodos }) => {


    const handleEditTodo = (id) => {
       const editedTodo = prompt("Edit Todo: ")
        const updatedTodos = todos.map(todo => {
            if (todo.id !== id){
                return todo
            }else{
                return {
                    id: id,
                    text: editedTodo
                }
            }
        })
        setTodos(updatedTodos)     
    }

    const handleDeleteTodo = (id) => {
        const remainingTodos = todos.filter(todo => id !== todo.id)
        setTodos(remainingTodos)
    }

    return (
        <div>
            {
                todos.map(todo => {
                    return (
                        <div>
                            <h4>{todo.text}</h4>
                            <button onClick={() => handleEditTodo(todo.id)}>Edit</button>
                            <button onClick={() => handleDeleteTodo(todo.id)}>Delete</button>
                        </div>  
                    )
                })
            }
        </div>
    )
}

export default TodoList