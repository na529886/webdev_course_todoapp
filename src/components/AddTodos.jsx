import React from 'react'



const AddTodos = ({ input, setInput, setTodos, todos }) => {

    const todoLength = todos.length
    console.log(todoLength)


    return (
        <div>
           <input type="text" value={input} onChange={(e) => setInput(e.target.value)}/>
           <button onClick={() => setTodos([...todos, {id: todoLength, text: input}])}>Add Todo</button>
        </div>
    )
}

export default AddTodos